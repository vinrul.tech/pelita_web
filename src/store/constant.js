const defaultState = {
    title: "Pelayanan Inspiratif Terpercaya"
}

const actions = {
    set_title: (context, title) => {
        context.commit('TITLE', title)
    }
}

const mutations = {
    TITLE: (state, title) => {
        state.title = title;
        
        
    }
}

const getters = {
    title: (state) => {
        return state.title;
    }
}

export default {
    namespaced: true,
    state: defaultState,
    getters,
    actions,
    mutations,
}