import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueFriendlyIframe from 'vue-friendly-iframe';
import Default from './layouts/default'
import Child from './layouts/child'
import NotFound from './layouts/notfound'
import './assets/style.css'

import VuePreviewPDF from 'vue-preview-pdf'

Vue.use(VuePreviewPDF)


Vue.component('default-layout', Default)
Vue.component('child-layout', Child)
Vue.component('notfound-layout', NotFound)

Vue.use(VueFriendlyIframe);

Vue.config.productionTip = false

import router from './router'
import store from './store'

Vue.mixin({
  methods: {
    breadcumText: (str) => {
      const text = str.replace("_", " ");
      //return text.charAt(0).toUpperCase() + text.slice(1)
      return text
        .toLowerCase()
        .split(' ')
        .map(word => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ');
    }
  }
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
