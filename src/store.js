import Vue from 'vue'
import Vuex from 'vuex'


import constant from './store/constant';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        constant,
    }
});