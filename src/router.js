import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)



let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'home',
      meta: {
        layout: 'default'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/home.vue')
    },
    {
      path: '/publik',
      name: 'publik',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik.vue')
    },
    {
      path: '/internal',
      name: 'internal',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal.vue')
    },
    {
      path: '/kabupaten',
      name: 'kabupaten',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten.vue')
    },
    {
      path: '/prisma',
      name: 'prisma',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/prisma.vue')
    },
    {
      path: '/under_construction',
      name: 'under_construction',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/under.vue')
    },
    {
      path: '/publik/ptsp',
      name: 'publik-ptsp',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/ptsp.vue')
    },
    {
      path: '/publik/sipadu',
      name: 'publik-sipadu',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/sipadu.vue')
    },
    {
      path: '/publik/sitaga',
      name: 'publik-sitaga',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/sitaga.vue')
    },
    {
      path: '/publik/simanis',
      name: 'publik-simanis',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/simanis.vue')
    },
    {
      path: '/publik/lamak',
      name: 'publik-lamak',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/lamak.vue')
    },
    {
      path: '/publik/simpar',
      name: 'publik-simpar',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/simpar.vue')
    },
    {
      path: '/publik/tangan_umat',
      name: 'publik-tangan_umat',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/tangan_umat.vue')
    },
    {
      path: '/publik/panenpa',
      name: 'publik-panenpa',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/panenpa.vue')
    },
    {
      path: '/publik/siap_sapa',
      name: 'publik-siap_sapa',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/siap_sapa.vue')
    },
    {
      path: '/publik/rumah_binar',
      name: 'publik-rumah_binar',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/rumah_binar.vue')
    },
    {
      path: '/publik/medika',
      name: 'publik-medika',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/medika.vue')
    },
    {
      path: '/publik/sukma_podcast',
      name: 'publik-sukma_podcast',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/sukma_podcast.vue')
    },
    {
      path: '/publik/sitiba',
      name: 'publik-sitiba',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/publik/sitiba.vue')
    },
    {
      path: '/internal/jin_tomang',
      name: 'internal-jin_tomang',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal/jin_tomang.vue')
    },
    {
      path: '/internal/sesari',
      name: 'internal-sesari',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal/sesari.vue')
    },
    {
      path: '/internal/notip',
      name: 'internal-notip',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal/notip.vue')
    },
    {
      path: '/internal/simdak',
      name: 'internal-simdak',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal/simdak.vue')
    },
    {
      path: '/internal/tpki',
      name: 'internal-tpki',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal/tpki.vue')
    },
    {
      path: '/internal/tpke',
      name: 'internal-tpke',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal/tpke.vue')
    },
    {
      path: '/internal/smart',
      name: 'internal-smart',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal/smart.vue')
    },
    {
      path: '/internal/absensi',
      name: 'internal-absensi',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal/absensi.vue')
    },
    {
      path: '/internal/kantinku',
      name: 'internal-kantinku',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/internal/kantinku.vue')
    },
    {
      path: '/kabupaten/siopa',
      name: 'kabupaten-siopa',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten/siopa.vue')
    },
    {
      path: '/kabupaten/puspa',
      name: 'kabupaten-puspa',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten/puspa.vue')
    },
    {
      path: '/kabupaten/pinter',
      name: 'kabupaten-pinter',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten/pinter.vue')
    },
    {
      path: '/kabupaten/puspaka',
      name: 'kabupaten-puspaka',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten/puspaka.vue')
    },
    {
      path: '/kabupaten/lentera',
      name: 'kabupaten-lentera',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten/lentera.vue')
    },
    {
      path: '/kabupaten/pasupati',
      name: 'kabupaten-pasupati',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten/pasupati.vue')
    },
    {
      path: '/kabupaten/sila',
      name: 'kabupaten-sila',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten/sila.vue')
    },
    {
      path: '/kabupaten/sopasti',
      name: 'kabupaten-sopasti',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten/sopasti.vue')
    },
    {
      path: '/kabupaten/simpati',
      name: 'kabupaten-simpati',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kabupaten/simpati.vue')
    },
    {
      path: '/prisma/padma',
      name: 'prisma-padma',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/prisma/padma.vue')
    },
    {
      path: '/prisma/postman',
      name: 'prisma-postman',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/prisma/postman.vue')
    },
    {
      path: '/prisma/silma',
      name: 'prisma-silma',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/prisma/silma.vue')
    },
    {
      path: '/prisma/siloma',
      name: 'prisma-siloma',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/prisma/siloma.vue')
    },
    {
      path: '/prisma/sioma',
      name: 'prisma-sioma',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/prisma/sioma.vue')
    },
    {
      path: '/zi',
      name: 'zi',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/zi.vue')
    },
    {
      path: '/zi/dokter_zi',
      name: 'zi-dokter_zi',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/zi/dokter_zi.vue')
    },
    {
      path: '/kua',
      name: 'kua',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/kua.vue')
    },
    {
      path: '/about',
      name: 'about',
      meta: {
        layout: 'child'
      },
      component: () => import( /* webpackChunkName: "about" */ './views/about.vue')
    },
  ]
});



export default router;